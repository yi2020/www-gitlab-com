---
layout: markdown_page
title: "Category Direction - Code Testing and Coverage"
canonical_path: "/direction/verify/code_testing/"
---

- TOC
{:toc}

## Code Testing

Code testing and coverage ensure that individual components built within a pipeline perform as expected. This is a core piece of the Ops Section [direction](/direction/ops/#smart-feedback-loop) "Smart Feedback Loop" between developers and were we are aiming to make that as [reliably speedy](/direction/ops/#speedy-reliable-pipelines) as possible, eventually enabling users to go from first commit to code in production in only an hour with confidence.

- [Maturity Plan](#maturity-plan)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ACode%20Testing%20and%20Coverage)
- [Overall Vision](/direction/ops/#verify)

Interested in joining the conversation for this category? Please join us in the issues where we discuss this topic and can answer any questions you may have. Your contributions are more than welcome.

This page is maintained by the Product Manager for Testing, James Heimbuck ([E-mail](mailto:jheimbuck@gitlab.com))

## What's Next & Why
Test failures are not all the same and understanding how often a test has failed recently can reduce the amount of time to debug and fix it which is why we are adding the [Test Failure Indicator](https://gitlab.com/gitlab-org/gitlab/-/issues/235525) to the Unit Test report next. This is a first step towards a richer view of test history and [flaky tests in a project](https://gitlab.com/gitlab-org/gitlab/-/issues/3673).

Users are excited to have data and a visual indicator of the direction code coverage is trending in a project. For users that have dozens to thousands of projects though having an even higher level view of coverage is often required, especially in the enterprise. The last step to deliver our vision for [Code Coverage for groups](https://gitlab.com/groups/gitlab-org/-/epics/2838) is to implement the [graph of the average coverage](https://gitlab.com/gitlab-org/gitlab/-/issues/215140) for all projects in a group.

## Who we are focusing on? 

Check out our [Ops Section Direction "Who's is it for?"](/direction/ops/#who-is-it-for) for an in depth look at the our target personas across Ops. For Code Testing and Coverage, our "What's Next & Why" are targeting the following personas, as ranked by priority for support: 
1. [Delaney - Development Team Lead](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#delaney-development-team-lead)
1. [Sasha - Software Developer](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#sasha-software-developer)
1. [Simone - Software Engineer in Test](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#simone-software-engineer-in-test)
1. [Devon - DevOps Engineer](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)

## Maturity Plan

This category is currently at the "Viable" maturity level, and our next maturity target is "Complete" (see our [definitions of maturity levels](/direction/maturity/)). Key deliverables to achieve this are included in these epics:

* [Code Coverage Data for groups](https://gitlab.com/groups/gitlab-org/-/epics/2838)
* [Test History for MRs and Pipelines](https://gitlab.com/groups/gitlab-org/-/epics/4155)
* [Historic Test Data for projects](https://gitlab.com/groups/gitlab-org/-/epics/3129)

We may find in research that only some of the issues in these epics are needed to move the vision for this category maturity forward. The work to move the maturity is captured and being tracked in [this epic](https://gitlab.com/groups/gitlab-org/-/epics/3660).

## Competitive Landscape

Many other CI solutions can also consume standard JUnit test output or other formats to display insights natively like [CircleCI](https://circleci.com/docs/2.0/collect-test-data/) or through a plugin like [Jenkins](https://plugins.jenkins.io/junit). 

There are new entries in the code testing space utilizing ML/AI tech to optimize test execution like [Launchable](https://launchableinc.com/solution/) and even write test cases like [Diffblue](https://www.diffblue.com/).

In order to stay remain ahead of these competitors we will continue to push forward to make unit test data visible and actionable in the context of the Merge Request for developers with [unit test reports](https://docs.gitlab.com/ee/ci/unit_test_reports.html#viewing-unit-test-reports-on-gitlab) and historical insights to identify flaky tests with issues like [gitlab#33932](https://gitlab.com/gitlab-org/gitlab/issues/33932).

## Top Customer Success/Sales Issue(s)

Sales has requested a higher level view of testing and coverage data for both projects and groups from the Testing Group. A first step towards this will be the display of [coverage data for groups](https://gitlab.com/groups/gitlab-org/-/epics/2838).

## Top Customer Issue(s)

The most popular issue(s) in the Code Testing and Coverage category today are requests to be able to enforce [a code coverage increase or hold](https://gitlab.com/gitlab-org/gitlab/-/issues/15765) or [a coverage percentage](https://gitlab.com/gitlab-org/gitlab/-/issues/6284) before a merge. 

Another popular issue is a request to see the [code coverage badge on any branch](https://gitlab.com/gitlab-org/gitlab/-/issues/27093) which would solve common problem for users of long lived branches who do not have a view of the test coverage of those branches today.

## Top Internal Customer Issue(s)

The GitLab Quality team has opened an interesting issue, [Provide API to retrieve test case durations from a pipeline](https://gitlab.com/gitlab-org/gitlab/issues/14954), that is aimed at solving a problem where they have limited visibility into long test run times that can impact efficiency.

## Top Analyst Landscape Items

In 2020, Gartner has released the Artificial Intelligence Use Case Prism for Development and Testing on their [research website](https://www.gartner.com/en/documents/3994888/infographic-artificial-intelligence-use-case-prism-for-d). Directionally, several of the use cases are generation of unit tests from analyzing code patterns, using business logic to create API test scenarios, and using machine learning to fabricate test data as well as correlating testing results back to business metrics to convey meaningful connections like release success or quality. 

## Top Vision Item(s)
To realize our long term vision we need to add more value not just for users uploading junit.xml and Cobertura reports but for any users with test and coverage reports. We believe that the best way to do this is to make it easy for users to contribute additional parsers so they can access the features the team is building that use the data. This will allow wider community contributions and is in alignment with [GitLab's Dual Flywheel strategy](https://about.gitlab.com/company/strategy/#dual-flywheels). A first step towards this could be a [GitLab-specific unit test report](https://gitlab.com/gitlab-org/gitlab/-/issues/247975).

We are also looking to provide a one stop place for CI/CD leaders with [Director-level CI/CD dashboards](https://gitlab.com/gitlab-org/gitlab/issues/199739). Quality is an important driver for improving our users ability to confidently track deployments with GitLab and so we are working next on a view of code coverage data over time for across a [group's projects](https://gitlab.com/groups/gitlab-org/-/epics/2838).

We have started brainstorming some ideas for the vision and captured that as a rough design idea you can see below.

![Design for Vision of Code Testing and Coverage data summary](/images/code-testing-data-view-vision.png)

