---
layout: handbook-page-toc
title: Product develoment flow releases
---


**13.8 (2021-01-22)**

- Incorporate final feedback and close remaining issues on the [PD-FLOW Working group board](https://gitlab.com/gitlab-com/www-gitlab-com/-/boards/2012861?label_name[]=wg-product-development-flow)
- General GitLab team contributions

[Create new directory and handbook page for success metrics of PD Flow](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/72532)

[Add Design Sprint Template to issue templates](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/51226)

[Clarify ownership of maintaining issues as SSOT](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/71492)

[Linked to iteration strategies where iteration was mentioned](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/71213)

[Update pdflow page change instructions to reflect latest process needs](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/71006)

[Product development flow releases page](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/70796)

[Add validation spectrum to PDF](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/70793)

[Remove workflow::production label from Launch phase of pd-flow](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/70391)


**13.7 (2020-12-22)**

- Incorporate feedback from 13.6 Gitlab [dogfooding](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/9453) and [13.5 engineering review feedback](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/9263)
- Various topics including clarification of content, removal of optional labels and addressing processes for teams without SETs

[Add additional information to explain why the PDF isn't waterfall](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/70210)

[Issue refinement suggestions for PD-workflow](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/70706)

[Incorporate Current Quad Planning process as part of the new pd flow](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/9130)

[Clarify or remove workflow::scheduling and workflow::ready for development](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/9252)

[Move issue reviews from pdflow to product process page](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/70247)

[Make workflow diagram clickable](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/9813)

[Casing Consistency follow-up from "Draft: Product Development Flow v1.1 (Milestone 13.6) Changelog"](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10095)

[Add responsibilities of a appsec engineer in Build phase 2](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/69584)

[Testing strategy for groups with no SET](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/69579)

[Update intro to better reflect goal of latest pd-flow version](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/70684)

[Product Development Flow: Add `workflow::refinement` as optional step to build phase 1](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/70388/diffs)

[Product Development Flow: standardized job titles](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/70389)


**13.6 (2020-11-22)** 

- Introduce interactive/improved visuals for workflow table and required items*
- Clarify required outcomes and optimize resources/links for activities as well as alternate issues types such as bugs
- Find opportunities to reduce/remove content from the page to create better focus on the flow
- Review and improve cross-functional from TWs, UXRs and stakeholders

[Product Development Flow v1.1 (Milestone 13.6) Changelog](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/69131)

**13.5 (2020-10-22)** 

- New structure and core content to shift to an outcomes/activities model. 
- Introduced key participants table to clarify collaborators at each phase
- Replaced specific step by step instructions in each phase with potential activities to achieve specific outcomes
- Specified what’s required in each phase to labels and outcomes, leveraging Required 🔎

[Product Development Flow v1 Beta (Milestone 13.5) Changelog](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/66118)

