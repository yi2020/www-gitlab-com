---
layout: handbook-page-toc
title: "Create:Code Review BE Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The Create:Code Review BE team is responsible for all backend aspects of the product categories that fall under the [Code Review group][group] of the [Create stage][stage] of the [DevOps lifecycle][lifecycle].

[group]: /handbook/product/categories/#code-review-group
[stage]: /handbook/product/categories/#create-stage
[lifecycle]: /handbook/product/categories/#devops-stages

## Team Members

The following people are permanent members of the Create:Code Review BE Team:

<%= direct_team(manager_role: 'Backend Engineering Manager, Create:Code Review') %>

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(role_regexp: /[,&] (Create(?!:)|Create:Code Review)/, direct_manager_role: 'Backend Engineering Manager, Create:Code Review') %>

## Sisense & KPIs
<%#= TODO: Ensure dashboard exists at https://app.periscopedata.com/app/gitlab/561630/Dev-Sub-department-Overview-Dashboard #>
<%#= partial("handbook/engineering/development/dev/create-code-review-be/metrics.erb") %>
Coming Soon

## Hiring

This chart shows the progress we're making on hiring. Check out our
[jobs page](/jobs/) for current openings.

<%= hiring_chart(department: 'Create:Code Review BE Team') %>


## Work

In general, we use the standard GitLab [engineering workflow]. To get in touch
with the Create:Code Review BE team, it's best to create an issue in the relevant project
(typically [GitLab]) and add the `~"group::code review"` and `~backend` labels, along with any other
appropriate labels. Then, feel free to ping the relevant Product Manager and/or
Engineering Manager as listed above.

For more urgent items, feel free to use [#g_create_code_review] on Slack.

[Take a look at the features we support per category here.](/handbook/product/categories/features/#createcode-review-group)

[engineering workflow]: /handbook/engineering/workflow/
[GitLab]: https://gitlab.com/gitlab-org/gitlab
[#g_create_code_review]: https://gitlab.slack.com/archives/g_create_code-review

### Working with product

Weekly calls between the product manager and engineering managers (frontend and backend) are listed in the "Code Review Group" calendar. Everyone is welcome to join and these calls are used to discuss any roadblocks, concerns, status updates, deliverables, or other thoughts that impact the group. Every 2 weeks (in the middle of a release), a mid-milestone check-in occurs, to report on the current status of `~"Deliverable"`s. Monthly calls occurs under the same calendar where the entire group is encouraged to join, in order to highlight accomplishments/improvements, discuss future iterations, review retrospective concerns and action items, and any other general items that impact the group.

### Collaborating with other counter parts

You are encouraged to work as closely as needed with stable counterparts outside of the PM. We specifically include quality engineering and application security counterparts prior to a release kickoff and as-needed during code reviews or issue concerns.

Quality engineering is included in our workflow via the [Quad Planning Process](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6318).

Application Security will be involved in our workflow at the same time that [kickoff emails](#kickoff-emails) are sent to the team, so that they are able to review the upcoming milestone work, and notate any concerns or potential risks that we should be aware of.


### Capacity planning

<%= partial("handbook/engineering/development/dev/create/capacity_planning.erb") %>

#### Availability

Approximately 5-10 business days before the start of a new release, the EM will begin determining how "available" the team will be. Some of the things that will be taken into account when determining availability are:

* Upcoming training
* Upcoming time off / holidays
* Upcoming on-call slots
* Potential time spent on another teams deliverables

Availability is a percentage calculated by _(work days available / work days in release) * 100_.

All individual contributors start with a "weight budget" of 10, meaning they are capable (based on historical data) of completing a maximum number of issues worth 10 weight points total (IE: 2 issues which are weighted at 5 and 5, or 10 issues weighted at 1 each, etc.) Then, based on their availability percentage, weight budgets are reduced individually. For example, if you are 80% available, your weight budget becomes 8.

Product will prioritize issues based on the teams total weight budget. Our [planning rotation](#planning-rotation) will help assign weights to issues that product intends on prioritizing, to help gauge the amount of work prioritized versus the amount we can handle prior to a kickoff.

#### Kickoff emails

Once availability has been determined, weights have been assigned, and the PM/EM finalize a list of prioritized issues for the upcoming release, kickoff emails will be sent. The intent of this email is to notify you of the work we intend to assign for the upcoming release. This email will be sent before the release begins. The kickoff email will include:

* Your availability, weight budget, and how it was calculated
* A list of the issues you will most probably be assigned as an individual
* A reasoning behind why you have been assigned more than your weight budget, if applicable
* A list of the issues the team is working on that are deemed "note-worthy," in case you'd like to offer help on those issues as time allows

Emails get sent to each individual contributor on the team, as well as the Application Security counterpart, in order to give a heads-up about the upcoming issues in the milestone and what the assignments will be.

#### Planning

> If you haven't read the code, you haven't investigated deeply enough
>
> -- <cite>Nick Thomas</cite>

To assign weights to issues in a future milestone, on every 4th of the development month (or the next working day if it falls on a holiday or weekend), BE engineers look at the list of issues that are set for next milestone. These are assigned by the engineering manager. To weight issues, before the 15th of the month, they should:

1. See if there is already a discussed backend solution/plan or none yet.
1. If the discussed backend solution/plan isn't that clear, clarify it.
1. If there's no solution/plan yet, devise one. Doesn't need to be a detailed solution/plan. Feel free to ask other people to pick their brains.
1. If there's a need to collaborate with stable counterpart to devise a solution/plan, add a comment and tag relevant counterparts.
1. Give issue a weight if there's none yet or update if the existing weight isn't appropriate anymore. Leave a comment why a certain weight is given.
1. It's strongly encouraged to spend no more than 2 hours per issue. Give it your best guess and move on if you run out of time.
1. Label the issue as ~"workflow::ready for development".
1. Unassign yourself or keep it assigned if you want to work on the issue.

#### Follow-up issues

You will begin to collect follow-up issues when you've worked on something in a release but have tasks leftover, such as technical debt, feature flag rollouts or removals, or non-blocking work for the issue. For these, you can address them in at least 2 ways:
* Add an appropriate future milestone to the follow-up issue(s) with a weight and good description on the importance of working this issue
* Add the issue(s) to the relevant [planning issue](https://gitlab.com/gitlab-org/create-stage/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acode%20review&search=planning)
 
You should generally take on follow-up work that is part of our [definition of done](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#definition-of-done), preferably in the same milestone as the original work, or the one immediately following. If this represents a substantial amount of work, bring it to your manager's attention, as it may affect scheduling decisions.

If there are many follow-up issues, consider creating an epic.

#### Backend and Frontend issues

Many issues require work on both the backend and frontend, but the weight of that work may not be the same. Since an issue can only have a single weight set on it, we use scoped labels instead when this is the case: `~backend-weight::<number>` and `~frontend-weight::<number>`.

### What to work on

<%= partial("handbook/engineering/development/dev/create/what_to_work_on.erb", locals: { group: "Code Review", slack_channel: 'g_create_code-review' }) %>

[issue board]: https://gitlab.com/groups/gitlab-org/-/boards/412126?label_name[]=group::code%20review&label_name[]=backend
[assignment board]: https://gitlab.com/groups/gitlab-org/-/boards/2142016

### Workflow labels

<%= partial("handbook/engineering/development/dev/create/workflow_labels.erb", locals: { group_label: 'group::code review' }) %>

### Async standup

<%= partial("handbook/engineering/development/dev/create/async_standup.erb") %>

Our team is encouraged to post links to their deliverable issues or merge requests when they are mentioned in relation to the second question. This helps other team members to understand what others are working on, and in the case that they come across something similar in the future, they have a good reference point.

### Retrospectives

We have 1 regularly scheduled "Per Milestone" retrospective, and can have ad-hoc "Per Project" retrospectives.

#### Per Milestone

<%= partial("handbook/engineering/development/dev/create/retrospectives.erb", locals: { group: "Code Review", group_slug: 'code-review' }) %>

#### Per Project

If a particular issue, feature, or other sort of project turns into a particularly useful learning experience, we may hold a synchronous or asynchronous retrospective to learn from it. If you feel like something you're working on deserves a retrospective:
1. [Create an issue](https://gitlab.com/gl-retrospectives/create-stage/code-review/issues) explaining why you want to have a retrospective and indicate whether this should be synchronous or asynchronous
2. Include your EM and anyone else who should be involved (PM, counterparts, etc)
3. Coordinate a synchronous meeting if applicable

All feedback from the retrospective should ultimately end up in the issue for reference purposes. %>

### Deep Dives

<%= partial("handbook/engineering/development/dev/create/deep_dives.erb") %>

### Career development

<%= partial("handbook/engineering/development/dev/create/career_development.erb", locals: { group: "Code Review" }) %>

### Performance Monitoring

The Create:Code Review BE team is responsible for keeping some API endpoints and
controller actions performant (e.g. below our target speed index).

Here are some Kibana visualizations that give a quick overview on how they perform:

Coming Soon

<%#= TODO: These must be split first %>
<%#= - [Create::Source Code: Controller Actions](https://log.gprd.gitlab.net/app/kibana#/visualize/edit/32698f60-b145-11ea-bfe2-25f984e253f8?_g=(filters%3A!()%2CrefreshInterval%3A(pause%3A!t%2Cvalue%3A0)%2Ctime%3A(from%3Anow-7d%2Cto%3Anow)))
- [Create::Source Code: Endpoints](https://log.gprd.gitlab.net/app/kibana#/visualize/edit/104d4bf0-a0d9-11ea-8cfd-8dcd98a55a1d?_g=(filters%3A!()%2CrefreshInterval%3A(pause%3A!t%2Cvalue%3A0)%2Ctime%3A(from%3Anow-7d%2Cto%3Anow)))

%> 

These tables are filtered by the endpoints and controller actions that the group
handles and sorted by P90 (slowest first) for the last 7 days by default.

