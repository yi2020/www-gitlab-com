---
layout: handbook-page-toc
title: "Creating and accessing your account"
description: “Discover how to create and access your GitLab Demo account”
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Overview

To get started with the GitLab Demo Cloud, you will need to create an account on our GitLab Demo Portal at [https://gitlabdemo.com](https://gitlabdemo.com). After you create your account, you can access your dashboard that provides access to all of the resources in the GitLab Demo Cloud.

This tutorial shows you how to create your account, access the GitLab instance, access integrations, and explore the catalog.

## Step-by-Step Instructions

### Task 1. Creating Your Demo Cloud Account

1. Open a **new tab or window** in your web browser.
2. Visit [https://gitlabdemo.com](https://gitlabdemo.com).
3. In the top right corner, click the **Register** link.
4. **Fill in the fields** in the registration form and click the **Register** button to create your account.
    * **GitLab Handle:** The GitLab handle is used for creating your username and group name on our GitLab instance. You can use the same handle as you use on GitLab.com, however the accounts are not related.
    * **Password:** To keep your account secure, we recommend using 1Password to randomly generate a password with at least 12 characters. For security reasons, do not use the same password that you use for GitLab.com.
5. Please **check your email** for the link to verify your account.

### Task 2. Accessing the GitLab Instance

When your account is created, the GitLab handle that you provide is used to automatically provision a user account and organizational group (with owner permissions) on the GitLab instance that we use in our demo environment.

> These instructions are focused on the US region, however the same steps apply if you would like to access the EU region.

1. If you're not already signed in to the GitLab Demo Portal, visit [https://gitlabdemo.com](https://gitlabdemo.com) and sign in using the account that you created.
2. On the Dashboard, you will see all of the Demo Cloud resources that you have access to.
3. Locate the **GitLab Omnibus (US)** card (bordered section).
    > When you created your account on gitlabdemo.com, your account on the Demo Cloud GitLab instance was automatically created using the GitLab handle that you chose and the same password as you used for gitlabdemo.com.

    ![GitLab Omnibus Card](https://storage.googleapis.com/gitlab-demosys-docs-assets/tutorials/getting-started/creating-accessing-your-account-1.png)

4. The URL of the GitLab instance is provided for you to bookmark.
    > It is recommended to add the URL of the GitLab Omnibus instance as a website on your existing 1Password record for gitlabdemo.com.

5. If you click the **GitLab Dashboard** button, a new tab will open with the GitLab dashboard for your user account. If you're prompted to log in, please use the credentials that you created for [https://gitlabdemo.com](https://gitlabdemo.com) in Task 1.
6. If you click the **My Group** button, a new tab will open with the GitLab group that was created for you to create your projects in.
    > We have designed our GitLab instance to support easy collaboration for projects that are created in groups. Please consider always creating your project in your group and as a personal project unless necessary.

### Task 3. Accessing Integrations

On the GitLab Demo Portal Dashboard, you will see all of the available integrations for the Demo Cloud below the GitLab Omnibus instance. You can use the provided credentials to access the integration. Please see the tutorials to learn more about how to use the integrations.

[Explore Integration Tutorials](/handbook/customer-success/demo-systems/tutorials/integrations)

### Task 4. Accessing the Catalog

We have a library of multimedia content for a variety of GitLab feature and use cases. The catalog allows you to explore, learn, and perform an on-demand demos without needing a real-time environment using YouTube videos, Google Slides, and Markdown content with step-by-step instructions.

<div class="panel panel-warning">
<div class="panel-heading">
Work-in-Progress
</div>
<div class="panel-body">
The catalog is a work-in-progress and is currently accepting contributions for content. We appreciate your patience while we're adding more to our libraries.
</div>
</div>

1. On the GitLab Demo Portal, click the **Catalog** link in the top navigation bar.
2. In the list of libraries, click **View Library** for a library topic that interests you.
3. In the list of categories, click **View Scenarios** for a topic that interests you.
4. In the list of scenarios for a category, click on the **title** of a scenario topic that interests you.
5. Any media that is available for this scenario will be shown.
6. In the top right corner, you will see a **dropdown list of available versions** and a **Contribute a Version** button.
    > You can switch between different versions of this scenario to see what media is available. If you have created or know of a great video or slides on this topic, you can use the Contribute a Version form to add more content to the library.
7. Use the previous and next **View Scenario** buttons to navigate to different topics.
8. You can also **use the breadcrumbs at the top of the page** to navigate up to the library and explore a different category.

## Review

You have successfully created your GitLab Demo Portal account and now have access to all of the GitLab Demo Cloud resources. You can get help with your account in the `#demo-systems` channel on Slack.
