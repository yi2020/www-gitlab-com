---
layout: handbook-page-toc
title: "Channel Sales"
description: "The purpose of this page is to provide the  GitLab Sales team insights on the GitLab channel partner community and how to best work with our partners"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
#### GitLab team members are the primary audience for this page.

# **GitLab Channel Overview**

The GitLab Partner Program enables partners — including systems integrators, cloud platform partners, independent software vendors, managed service providers, resellers, distributors and ecosystem partners — to maximize customer value through the GitLab platform and their value-added GitLab and DevOps services.   Although we welcome all partners, our program is structured to provide additional rewards for partners that make a commitment and investments in a deeper GitLab relationship.

The purpose of this page is to provide the GitLab Sales team insights on the GitLab channel partner community and how to best work with our partners.

Additional related channel resources:



*   [Channel Operations Page](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/) - Learn how to manage partner deals in Salesforce and other operational transaction processing tips when working with partners.
*   [Channel Partner Program page](https://about.gitlab.com/handbook/resellers/) - This page provides an overview of the GitLab channel partner program, the expectations of partners, benefits for partners and how partners can work with GitLab.
*   [Channel Services Program Page](https://about.gitlab.com/handbook/resellers/services/) - Provides information on the GitLab services program, training and partner practice development.
*   [Channel Training, Certification and Enablement Page ](https://about.gitlab.com/handbook/resellers/training/)- Documents available training and certifications for partners.  Lists out additional enablement resources for partners.
*   [Partner Locator](https://partners.gitlab.com/English/directory/) - Enables customer and GitLab team members to find a partner in a specific geographic location and with specific services skills.


### **Channel Value for GitLab**

The Channel is a critical part of our overall go to market strategy and are a valuable extension of the GitLab team.  Our channel partners help us scale sales, marketing and services engagement, and also help us reach new markets and decision makers.  Additionally, our partners help us:



1. Drive growth ARR through services capacity and capability to drive customer adoption and usage of the GitLab platform.
2. Drive and increase new customer ARR through their relationships, service engagements, and knowledge of accounts.


### **How can GitLab sellers benefit from our channel partners?**

GitLab channel partners have established sales forces that help us multiply our sales coverage. Through that coverage and deep customer relationships, partners can identify new customer opportunities and new sales opportunities with existing customers, resulting in ARR growth. Through their service offerings and vendor partnerships, they can deliver more complete solutions than GitLab can alone, and drive customer adoption of GitLab. As a result, the GitLab channel will:



1. Improve customer reach and experience
    1. Resellers/SI’s that know & can make intros to your customers
    2. Ease of order processing
2. Channel leverage
    3. Resellers/SI’s that bring us into new opportunities & grow existing
    4. Decreased Customer Acquisition Cost
    5. Improved Net & Gross Retention
3. Services Capacity to Adopt and Expand GitLab deployments
    6. SI’s that help our customers deliver more value
    7. Stage Monthly Active Users Acceleration
4. Market Position
    8. Acquire partners from the Atlassian/Cloud Bees channel
    9. Monetize the GitHub channel
    10. Align to the AWS channel


## **Channels Partner Types**


### Resellers

Primary monetization is through reselling GitLab licenses and services. Resellers can be a service partner too (often known as a Solution Provider).



1. VAR (Value Added Reseller): Channel services including resale, implementation, contracting, support, financing etc.
2. DMR (Direct Market Reseller): Primary business is resale of the software, often does not implement. Value are the contracts that these partners have in place with customers.
3. Distributor.  Help us scale our business by leading relationships with our Open partners worldwide.  Distributors hep us recruit, onboard, transact with and support services for partners.


### Services Partners

Primary monetization is through the sale of a partner’s own GitLab and devops services. This can be a one-time implementation, ongoing support or advisory, managed services, or outsourcing. Services partners will resell GitLab services, deliver services on behalf of GitLab or deliver GitLab certified services. Services partners can be a reseller partner too (often known as a Solution Provider).



1. Global Systems Integrators have a large global workforce and can deliver on almost any customer need. Examples: Accenture, Deloitte, Contino
2. Regional Systems Integrators are large workforce but with single continental focus and a more limited offering of services. Examples: CI&T, WWT
3. Boutique Systems Integrators are very focused DevOps partners that could be deep experts on GitLab and the nuances of getting it setup and running it. Examples - D-Ops and CPrime
4. Managed Service Providers provide ongoing support for solutions/applications. Examples: 2nd Watch, Rackspace


### Alliances

Learn more about our Alliance partners by visiting the [Alliances](https://about.gitlab.com/handbook/alliances/)<span style="text-decoration:underline;"> Handbook page.</span>


## **Channels Partner Tracks**


### Select

Select partners are by invitation only and are reserved for partners that make a greater investment in GitLab expertise, develop services practices around GitLab and are expected to drive greater GitLab product recurring revenues.



*   GitLab Channel Sales Managers focus most of their time on thse partners
*   We expect them to invest in services practices
*   Develop joint business plans
*   Earn greater discounts in return for greater investments.
*   Expected to be devops experts


### Open

Open - Resellers, integrators and other sales and services partners join the program in the Open track. Open is for all Partners in DevOps space, or ItaaS and other adjacent spaces that are committed to investing in their DevOps practice buildout. Also the Open track is for Partners seeking to develop customers or just want to learn about Gitlab and participate in the Gitlab partner community. GitLab Open Partners may or may not be transacting partners, and can earn products discounts or referral fees.

## Working with GitLab Channel Partners

### Partners can help you scale and working with them is comp neutral

[Channel Neutral Compensation](https://about.gitlab.com/handbook/sales/commissions/#channel-neutral-compensation)

To incentivize working with our Channel partners, 'Channel Neutral' means that we will not reduce $ value to individual sellers even if the Channel business reduces total iACV to GitLab (via discounts or rebates). More information can be found on the [compensation page](https://about.gitlab.com/handbook/sales/commissions/#channel-neutral-compensation).

FY21 commissions will be channel neutral for all deals through partners (including Distributors, if applicable), which means standard partner discounts are credited back to the salesperson and must follow [order processing procedures](https://about.gitlab.com/handbook/business-ops/order-processing/) to correctly flag partner attribution. Total IACV on the deal after all discounts will count towards quota credit, but the channel neutral amount does not qualify for quota credit and only pays out for compensation at BCR. See Channel Neutral section referenced in the [FY21 Commission Plan presentation](https://docs.google.com/presentation/d/1lPZAmdHPPJwhJ4felwsgDXopIt7Usn3UnK5siOpQMlY/edit#slide=id.g7da31a6494_6_0)

Partner Services also qualify for [FY21 Professional Services Spiff (v2)](https://about.gitlab.com/handbook/sales/commissions/#fy21-professional-services-spiff-v2). So as partners help customers adopt and expand their GitLab deployments, they can earn a Services Attach rebate and the GitLab sales team can earn the Professional Services Spiff.

### Maximizing value by working with Channel Partners

Developing successful relationships with channel partners in your region requires some effort, but has a great payback. Following are a few key tips that will help you develop relationships with GitLab channel partners and greatly increase your sales effectiveness.

Watch the August 2020 Level Up session [Channel Partner Success for GitLab Sellers](https://www.youtube.com/watch?v=OeykHQetd7U&feature=youtu.be) to learn tips on how to engage with our channel partners and maximize the value of your partner relationships.



*   **Honor Partner ←→ Customer Relationships**
    *   Respect Deal Reg’s from Partners: <span style="text-decoration:underline;">No</span> direct sales of opportunities with approved Partner Deal Reg without prior approval of VP, Global Channel Sales
    *   Recognize Incumbent Partners*: Do not bring another Partner into your customer convos
*   **For new Deal Reg’s in your pipeline**
    *   Be prepared to construct a simple joint pursuit plan in SFDC (CAMs are here to support same)
    *   Strive to team with the trained Partner people
*   **The program determines the partner discount**
    *   Our agreement with partners is that by initiating, assisting or fulfilling sales opportunities, or selling services, partners will earn set amount based on their partner track.
    *   These discounts are determined by the program and not GitLab Sales on individual opportunities.
*   **Respect incumbent partners**
    *   Support them in driving renewals and incremental sales
    *   Do not bring competing partners into the account
*   **Stay Focused on the Customer’s Needs & Priorities**
    *   Partners can complete a customer solution with additional products and services
*   **Give to Get (LevelUp coming)**
    *   Bring Select/Open partners into GitLab opportunities
    *   Ask them about opportunities they may have for GitLab
*   **Collaborate with Partners in your Region (LevelUp coming)**
    *   Treat them as an extension of your team
    *   Plan with key partners in your region
    *   Communicate regularly with them around sales opportunities and pipelines
    *   Help keep GitLab top of mind with them. Educate them on GitLab.
*   **If misalignments or escalations arise during deal pursuits**
    *   Contact the CAM
    *   Chat with your peers that have channel co-selling experience
    *   Escalate to your manager
    *   No customer collisions: Strive to stay aligned \ on customer calls - take differing POVs offline

### **Partner Co-selling best practices, and how Partners can help with R7 and Command plans**

In general Partners do not want to be told _generally_ “how to sell” but they **_do value _**tips and tricks about _selling GitLab_ (from your experience, what to watch out for, what are the effective sales motions, etc.). 

Partners often will have their own best practices selling methodologies in place, so it is <span style="text-decoration:underline;">not</span> recommended AEs expect Partners to embrace the entire R7 approach, nor build out an entire Command Plan. However **_Partners can be valuable_** for collecting and sharing key elements of both.  



*   As a best practice, with support of the Channel Mgr if/as needed, AEs should be sure the Partner is pre-briefed prior any joint call with the customer. 
*   Be sure the Partner is clear on the talk track and the desired outcomes of the call. 
*   One efficient way to do this is schedule a call with the Partner 10 minutes just before the joint customer call.
*   Of course if schedules do not allow that, a brief email to the partner should also be effective. 


### **Required 7 & Partner Co-selling**


<table>
  <tr>
   <td><strong>Required component</strong>
   </td>
   <td><strong>Detail</strong>
   </td>
   <td><strong>Partner Co-selling Guidance</strong>
   </td>
  </tr>
  <tr>
   <td><strong>Logging call notes with (ii) in the title</strong>
   </td>
   <td>Every verbal interaction with an account is logged.
   </td>
   <td>Whether AE captures directly the notes from Partner co-selling engagement calls/emails, or Chan Mgr collects and documents them separately and shares with  the AE, all call notes should be captured noting it’s ‘via the Partner’.  
   </td>
  </tr>
  <tr>
   <td><strong>Rank/tier all of your accounts</strong>
   </td>
   <td>Any customer with CARR higher than 0 or any prospect with an open opportunity needs to be tiered
   </td>
   <td>Partner/Chan Mgr may uncover customer information that would impact a customer’s tiering. Anything they learn will be promptly shared with the AE.   
   </td>
  </tr>
  <tr>
   <td><strong>Keep your next steps up to date</strong>
   </td>
   <td>Next steps must be up to date <span style="text-decoration:underline;">every day</span>
   </td>
   <td>To lay the foundation for success, always capture Partner tasks in the opp with a due date agreed upon by the Partner (at least a goal date). 
<p>
If a Partner task becomes overdue, The AE should reach out to the Partner & Channel Manager requesting an update. The Channel Manager becomes DRI to get an update from the Partner.
   </td>
  </tr>
  <tr>
   <td><strong>Your opportunities have to be up to date</strong>
   </td>
   <td>$ amount, opp name, type, stage & close dates must be accurate at all times
   </td>
   <td>The Partner and/or Channel Manager will proactively share key learnings/updates on deals that may be useful for the AE’s potential forecast update.
   </td>
  </tr>
  <tr>
   <td><strong>Command plans for all deals over $5k amount or $10k IACV (SMB) or $10k amount or $20k IACV (Mid-Market)</strong>
   </td>
   <td>Overview section must be completed for deals over $5k in SMB (AMOUNT, not IACV) or $10k in Mid-Market. Full for deals over $10k IACV in SMB or $20k in Mid-Market
   </td>
   <td>The AE is still DRI for Command plan construction <em>however, </em>the Partner may be able to collect and share inputs to some of its elements. The <a href="https://docs.google.com/document/d/1_Sm88mak9vs1pPMC_HhIE-1sq4Tv_NvDpUj_0GNsWbE/edit?usp=sharing">template</a> can be useful for this part of the co-selling motion. 
<p>
<strong>Regarding close plan: </strong>If this is a deal reg, often the Partner has sold to the customer below and will have knowledge of how this works.  
   </td>
  </tr>
  <tr>
   <td><strong>Custom pitch deck built for all opps over $5k amount (SMB) or $10k amount (Mid-Market)</strong>
   </td>
   <td><a href="https://about.gitlab.com/handbook/sales/commercial/#custom-deck-requirements">Requirements</a>
   </td>
   <td>As noted in other sections of R7 co-selling, Partners may be helpful collecting elements of the AE’s development of the customer pitch deck.  
   </td>
  </tr>
  <tr>
   <td><strong>Capture key information</strong>
   </td>
   <td>What is the customer’s understanding of what GitLab does should be captured in call notes. We also record why we won / lost a deal
   </td>
   <td>As noted in other sections of R7 co-selling, Partners may be helpful collecting and sharing key information. AEs should capture any learnings in the opportunity noted as ‘via Partner’    
   </td>
  </tr>
</table>

## **Partner Portal and Locator**

The GitLab Partner Portal is a resource for our channel partners that provides partners with enablement resources, as well as transaction entry and information.  On the partner portal, partners are able to access:



*   Deal registrations and opportunities
*   Training 
*   Enablement resources
*   Marketing resources
*   Program information
*   Calendar of partner events.

All of the partner account and contact data is available to GitLab teams in Salesforce.

The GitLab Partner Locator is a valuable resource for GitLab partners and customers.  The Locator, which can be found at about.gitlab.com/resellers, enables customers to find Authorized GitLab partners based several factors including:



*   Their location
*   Languages spoken
*   Services offered
*   Customer segments served.

Partners that invest the effort to develop a robust partner locator presence are able to differentiate themselves by highlighting their expertise and thought leadership.  

GitLab teams are able to use the Partner Locator to find partners in their region to help with specific customer opportunities with whom to develop a longer term relationship.


## **Channel Operations**

Information on managing channel opportunities, deal registrations and other valuable information about how to work with your partners can be found on the [Channel Operations page](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#welcome-to-the-channel-operations-page).


## **Definitions**

Program and incentive definitions can be found on the [Channel Operations page](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#program-and-incentive-definitions).


## **I need help! How to do I contact channel experts?**

The quickest way to get help is by using the following Slack channels:



*   #channel-sales for general sales questions about working with partners or finding help with a specific channel opportunity.
*   #channel-ops for questions regarding processing partner transactions.
*   #channel-services for help with engaging with services partners.
*   #alliances for help with engaging with alliances partners.
