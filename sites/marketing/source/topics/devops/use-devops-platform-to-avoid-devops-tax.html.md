---
layout: markdown_page
title: "Choose a DevOps platform to avoid the DevOps tax"
description: Too many tools/toolchains can impose a hefty DevOps tax on your organization. Here’s how a single DevOps platform can lighten your burden.
---

DevOps proves that there really can be too much of a good thing. By tying all the parts of the software development lifecycle together – from planning to delivery – it’s practically begging for tools to be cobbled together to do just that.

But, administering all these products and connecting them together is complex. For example, your CI needs to talk to your version control, your code review, your security testing, your container registry, and your configuration management. The permutations are staggering, and it’s not just a one-time configuration – each new project needs to reconnect all these pieces together.

This phenomenon is so real that it has a name: the DevOps tax. A DevOps tax is the price teams pay for using multiple tools and/or multiple toolchains in order to speed up the delivery of software. That price is often looked at in manpower spent: How much time does a team have to spend integrating and maintaining a toolchain versus actually coding and delivering software?

So what is a typical DevOps tax? A <a href="https://go.forrester.com/blogs/the-rise-fall-and-rise-again-of-the-integrated-developer-tool-chain/" target="_blank">Forrester Research report</a> from 2019 indicated it was approximately 10%, meaning 10% of the team had to support and maintain the toolchain. Our [2020 Global DevSecOps Survey](/developer-survey/) found it might be even higher: 22% of respondents said they spend between 11% and 20% of their time (monthly) supporting the toolchain.

The solution to this problem is a single DevOps platform, perhaps supported [by a platform team](/topics/devops/how-and-why-to-create-DevOps-platform-team.index.html), that will streamline every aspect of the software development lifecycle.

To avoid the DevOps tax, here’s what to consider: 

* Start with a single platform. Gartner Group forecasts that by 2023 40% of companies will standardize on a single DevOps platform (what Gartner currently refers to as a [DevOps value stream delivery platform](https://learn.gitlab.com/gartner-vsdp/gartner-mg-vsdp20). 

* Think about maintenance. How easy will it be to upgrade? Can upgrades be automated? How much manpower will it take to keep the platform running?

* Choose a platform with APIs in mind. A single DevOps platform doesn’t mean an organization will only have a single tool; in fact, most companies need to choose a platform that can be easily integrated with existing tools whether it’s a company-wide project management solution or something mandated by industry regulation. A DevOps platform with robust APIs for those types of integrations are a must. Ideally, a team should look for something with <a href="https://amazicworld.com/devops-tool-sprawl-is-tool-tax-just-the-tip-of-the-iceberg/" target="_blank">off-the-shelf integration capabilities</a>.

* Consider a “future-facing” platform. From IoT to AI and ML, exciting new technologies are just around the corner, so your DevOps platform needs to be able to accommodate that.

* Insist on a 360 degree view into everything. Gartner Group recommends platforms that offer “enhanced visibility, traceability, auditability, and observability” across the entire spectrum of operations.

* Don’t forget to support communication and collaboration. GitLab’s 2020 Survey found devs, security pros, ops team members, and testers were unanimous in their belief that communication and collaboration would be the most important skills for the future. Communication and collaboration are at the heart of so many stages of software development, from code review to UX and product planning, so choose a DevOps platform that support these efforts.

### Read more about DevOps:

The hidden costs of [DevOps toolchains](/webcast/simplify-to-accelerate/)

How BI Worldwide [leveraged a DevOps platform](/customers/bi_worldwide/)

Glympse went from [20 tools to one](/customers/glympse/)
