---
layout: job_family_page
title: "Professional Services Practice Manager"
---

## Job Grade 

The Professional Services Practice Manager is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

## Responsibilities

* Define, scope, and prioritize GitLab’s services and training offerings, including persona and market segmentation analysis, offering definition, scope, and pricing
* Collaborate with subject matter experts in the field, Product Management & Marketing, and instructional designers (as appropriate) to develop and deliver prioritized global services and training offerings, content, and sales & marketing collateral
* Ensure delivery model is focused on customer success outcomes while championing quality and efficiency
* Measure and report on the effectiveness of services and training enablement investments and programs
* Partner with sales team to drive bookings and closure of services and training engagements
* Ensure a robust closed feedback loop that embraces continuous improvement and iteration
* Identify and act on opportunities to improve the customer experience via innovative services/training offerings
* Build business case for additional services and training and enablement resources as needed

## Requirements

* Knowledge and familiarity with the Software Development Life Cycle and DevOps required (open source software knowledge and familiarity considered a plus)
* 3+ years relevant experience defining, developing, and executing customer services and/or training strategies, operations and action plans
* Ability to quickly understand technical concepts and explain them to customer and professional services audiences (mostly technical)
* Proven ability to effectively interact with and influence senior executives and team members
* Exceptional written/verbal communication and presentation skills
* Team player with strong interpersonal skills, skilled at project management and cross-functional collaboration
* Experienced in giving and receiving constructive feedback
* Ability to thrive in a fast-paced, unpredictable environment
* Share and work in accordance with GitLab's values
* Experience growing within a small start-up is a plus
* You share our values, and work in accordance with those values.
* [Leadership at GitLab](/company/team/structure/#director-group)
* Ability to use GitLab


## Performance Indicators
* [Bookings attached rate per agreed plan](/handbook/sales/#pcv)
* [Services bookings and revenue per agreed plan](/handbook/sales/#pcv)

## Specialties

### Professional Services & Consulting

## Responsibilities

- Serve in a player/coach role balancing field delivery with operations and strategy
- Establish strategic plans and operational practices (e.g., people, skill sets, engagement approaches, processes and policies, enablement and coaching) for existing and new services
- Develop collateral to clearly describe and communicate the customer value of the service offerings (i.e., scope, deliverables, pricing)
- Successfully introduce and incubate new services to deliver on key success measures, including customer-facing engagements and internal initiatives
- Partner with the sales teams to build services forecasts for new and existing services
- Help to manage resource assignments and staffing levels, including recruitment as needed
- Identify and implement process and tooling improvements to help project managers and consultants better support external customer professional services requirements
- Coach, motivate and train Professional Services Engineer team members and consultants to improve their delivery quality and efficiency
- Work with the Product Development and Support teams to contribute documentation for GitLab

### Training & Certification

## Responsibilities

* Develop, implement and monitor training programs.
* Evaluate needs of the company and align training with the organization’s strategic plans.
* Create training materials, online learning modules, presentations and multimedia visual aids.
* Evaluates the effectiveness of training based upon formal and informal feedback from customers and end users
* Conducts training sessions and develops criteria for evaluating the effectiveness of training activities
* Implements and manages a process to ensure that all training documentation, reports and data are completed and that materials are delivered per set guidelines
* Maintains and updates records of training activities, participant progress, and program effectiveness
* Maintains and updates skills by researching new training, educational, and multimedia technologies
* Regularly evaluates trainer’s professional skills and abilities and provides coaching and counseling as needed
* Build and manage infrastructure to support the training program.
* Assist with developing strategic plans to address the organization’s long-term training and career development needs.
* Supervise, coach and mentor staff.
* Collaborate with other departmental heads to define training needs and skills gaps.

## Requirements

* Bachelor’s degree in business, management, education or related field.
* 5+ years managing, specifically creating, training programs.
* Comprehensive knowledge of the principles, methods, and techniques used in the development and delivery of training programs
* Comprehensive knowledge of relevant training technologies, such as Learning Management Systems (LMS).
* Strong computer skills. Proficiency in Word, Excel and PowerPoint required. A working knowledge of core business systems preferred
* Excellent written and verbal communication skills with the ability to focus and clarify concepts
* Demonstrated problem solving and decision-making abilities with effective organizational and time management skills; the ability to handle multiple projects and priorities effectively in a fast-paced environment with minimal supervision
* Strong organizational, multi-tasking and presentation skills. Ability to create momentum and foster organizational change
* Must exhibit initiative, decisiveness and creativity, along with self-motivation and the ability to assume responsibility and maintain strict confidentiality
* Proven ability to conduct training needs assessments with key stakeholders.
* Proven ability to develop training curriculum and deliver effective and satisfying learning experiences.
* Experience with coaching and mentoring team members.
* You share our values, and work in accordance with those values.
* [Leadership at GitLab](/company/team/structure/#director-group)
* Ability to use GitLab

## Solutions Manager

## Responsibilities

- Define, scope, and prioritize GitLab’s services and training offerings, including persona and market segmentation analysis, offering definition, scope, and pricing
- Collaborate with subject matter experts in the field, Product Management & Marketing, and instructional designers (as appropriate) to develop and deliver prioritized global services and training offerings, content, and sales & marketing collateral
- Ensure services/training delivery model is focused on customer success outcomes while championing quality and efficiency
- Measure and report on the effectiveness of services and training enablement investments and programs
- Ensure a robust closed feedback loop that embraces continuous improvement and iteration
- Identify and act on opportunities to improve the customer experience via innovative services/training offerings
- Build business case for additional services and training and enablement resources as needed

## Requirements

- Knowledge and familiarity with the Software Development Life Cycle and DevOps required (open source software knowledge and familiarity considered a plus)
- 3+ years relevant experience defining, developing, and executing customer services and/or training strategies, operations and action plans
- Ability to quickly understand technical concepts and explain them to customer and professional services audiences (mostly technical)
- Proven ability to effectively interact with and influence senior executives and team members
- Exceptional written/verbal communication and presentation skills
- Team player with strong interpersonal skills, skilled at project management and cross-functional collaboration
- Experienced in giving and receiving constructive feedback
- Ability to thrive in a fast-paced, unpredictable environment
- Share and work in accordance with GitLab's values
- Experience growing within a small start-up is a plus
- Ability to use GitLab

### Senior Solutions Manager

The Senior Solutions Manager role extends the [Solutions Manager](#intermediate-requirements) role.

### Job Grade 

The Senior Professional Services Practice Manager is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

## Requirements
- 7+ years relevant experience defining, developing, and executing customer services and/or training strategies, tactics and action plans
- Advanced experience and insight with the GitLab platform
- Demonstrated experience leading others and partnering with senior executives
- Advanced ability to think strategically about business, products, and technical challenges
* You share our values, and work in accordance with those values.
* [Leadership at GitLab](/company/team/structure/#director-group)
* Ability to use GitLab

## Career Ladder

The next steps for the Professional Services Practice Manager Job Family would be to move to the [Director, Professional Services](/job-families/sales/director-of-professional-services/) Job family.

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.
1. Phone screen with a GitLab Recruiting team memeber 
2. Video Interview with the Hiring Manager
3. Team Interviews with 1-4 teammates 
Additional details about our process can be found on our [hiring page](/handbook/hiring).